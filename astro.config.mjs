import image from "@astrojs/image";
import tailwind from "@astrojs/tailwind";
import { defineConfig } from "astro/config";
import react from "@astrojs/react";
import node from "@astrojs/node";

import auth from "auth-astro";

// https://astro.build/config
export default defineConfig({
  site: "https://sacacitas.gitlab.io",
  // base: '/web/',
  integrations: [tailwind(), image({
    serviceEntryPoint: "@astrojs/image/sharp"
  }), react(), auth()],
  vite: {
    ssr: {
      external: ["svgo"]
    }
  },
  outDir: 'public',
  publicDir: 'static',
  adapter: node({
    mode: "standalone"
  }),
  output: 'server'
});
