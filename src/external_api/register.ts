
export const enum RegisterAnswerEnum {
    OK,
    USER_ALREADY_EXIST,
    WEAK_PASSWORD,
    ERROR
}


interface RegisterAnswer {
    status: RegisterAnswerEnum,
    message: any,
}

export async function register_user(email: String, password: String): Promise<RegisterAnswer> {
    const URL = import.meta.env.PUBLIC_API_BACKEND + "/auth/register";
    console.log(URL);


    return await fetch(URL, {
        method: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            email: email,
            password: password,
        })
    }).then(async res => {
        const resp = await res.json()

        console.log(resp)
        if (res.ok) {
            return {
                status: RegisterAnswerEnum.OK,
                message: ''
            } as RegisterAnswer
        }

        if (res.status == 400) {
            if (typeof resp.detail === "string") {
                return {
                    status: RegisterAnswerEnum.USER_ALREADY_EXIST,
                    message: ''
                } as RegisterAnswer
            }

            return {
                status: RegisterAnswerEnum.WEAK_PASSWORD,
                message: resp.detail.reason
            } as RegisterAnswer
        }

        return {
            status: RegisterAnswerEnum.ERROR,
            message: resp
        } as RegisterAnswer

    }).catch((err: any) => {
        return err
    })

}